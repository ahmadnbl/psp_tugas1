package com.nabeel.sipsbmptn.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import com.nabeel.sipsbmptn.model.Peserta;
import com.nabeel.sipsbmptn.model.Prodi;

@Mapper
public interface ProdiMapper {
	
	@Select("SELECT "
			+ "		p.kode_prodi, p.nama_prodi, u.kode_univ, u.nama_univ, u.url_univ "
			+ "FROM "
			+ "		prodi p "
			+ "LEFT JOIN univ u "
			+ "		ON u.kode_univ = p.kode_univ "
			+ "WHERE "
			+ "		p.kode_prodi = #{kodeProdi}")
	@Results(value = {
			@Result(property="kode_prodi", column="kode_prodi"),
			@Result(property="nama_prodi", column="nama_prodi"),
			@Result(property="Universitas.kode_univ", column="kode_univ"),
			@Result(property="Universitas.nama_univ", column="nama_univ"),
			@Result(property="Universitas.url_univ", column="url_univ")
			})
	Prodi getProdiDetail(@Param("kodeProdi") String kodeProdi);
	
	@Select("SELECT * FROM peserta WHERE kode_prodi = #{kodeProdi}")
	List<Peserta> getPesertaList(@Param("kodeProdi") String kodeProdi);
}
