package com.nabeel.sipsbmptn.service;

import com.nabeel.sipsbmptn.model.Peserta;

public interface PengumumanService {
	
	Peserta getPesertaStatus(String noPeserta);

}
