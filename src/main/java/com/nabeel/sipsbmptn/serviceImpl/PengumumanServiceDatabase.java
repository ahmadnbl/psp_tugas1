package com.nabeel.sipsbmptn.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nabeel.sipsbmptn.dao.PengumumanMapper;
import com.nabeel.sipsbmptn.model.Peserta;
import com.nabeel.sipsbmptn.service.PengumumanService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PengumumanServiceDatabase implements PengumumanService{
	
	@Autowired
	PengumumanMapper pengumumanMapper;

	@Override
	public Peserta getPesertaStatus(String noPeserta) {
		log.info("Mencari peserta dengan nomor " + noPeserta);
		Peserta peserta = pengumumanMapper.getHasilPengumumanByNomor(noPeserta);
		return peserta;
	}

}
