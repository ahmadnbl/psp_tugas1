package com.nabeel.sipsbmptn.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.AbstractMap.SimpleEntry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.nabeel.sipsbmptn.model.Peserta;
import com.nabeel.sipsbmptn.model.Prodi;
import com.nabeel.sipsbmptn.service.ProdiService;

@Controller
@RequestMapping("/prodi")
public class ProdiController {
	
	@Autowired
	ProdiService prodiServiceDatabase;
	
	@RequestMapping(method = RequestMethod.GET)
	public String showDetailProdi(Model model, 
			@RequestParam(value = "kode") String kodeProdi){
		model.addAttribute("kodeProdiInputted", kodeProdi);
		
		Prodi currentProdi = prodiServiceDatabase.getProdiDetail(kodeProdi);
		model.addAttribute("prodi", currentProdi);
		
		List<Peserta> listPeserta = new ArrayList<>();
		if(currentProdi != null){
			listPeserta.addAll(prodiServiceDatabase.getPesertaList(kodeProdi));
			SimpleEntry<Peserta, Peserta> youngestOldestPair = prodiServiceDatabase.getOldestAndYoungestPeserta(listPeserta);
			model.addAttribute("pesertaTermuda", youngestOldestPair.getKey());
			model.addAttribute("pesertaTertua", youngestOldestPair.getValue());
		}
		model.addAttribute("listPeserta", listPeserta);
		return "prodi/detail_prodi";
	}

}
