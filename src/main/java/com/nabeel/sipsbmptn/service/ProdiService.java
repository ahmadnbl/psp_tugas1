package com.nabeel.sipsbmptn.service;

import java.util.List;
import java.util.AbstractMap.SimpleEntry;

import com.nabeel.sipsbmptn.model.Peserta;
import com.nabeel.sipsbmptn.model.Prodi;



public interface ProdiService {
	
	Prodi getProdiDetail(String kodeProdi);
	
	List<Peserta> getPesertaList(String kodeProdi);
	
	SimpleEntry<Peserta, Peserta> getOldestAndYoungestPeserta(List<Peserta> listPeserta);

}
