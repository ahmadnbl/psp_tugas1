package com.nabeel.sipsbmptn.serviceImpl;

import java.util.AbstractMap.SimpleEntry;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nabeel.sipsbmptn.dao.ProdiMapper;
import com.nabeel.sipsbmptn.model.Peserta;
import com.nabeel.sipsbmptn.model.Prodi;
import com.nabeel.sipsbmptn.service.ProdiService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ProdiServiceDatabase implements ProdiService{
	
	@Autowired
	ProdiMapper prodiMapper;

	@Override
	public Prodi getProdiDetail(String kodeProdi) {
		log.info("Mengambil detail prodi, " + kodeProdi);
		return prodiMapper.getProdiDetail(kodeProdi);
	}

	@Override
	public List<Peserta> getPesertaList(String kodeProdi) {
		log.info("Mengambil daftar peserta dari prodi, " + kodeProdi);
		return prodiMapper.getPesertaList(kodeProdi);
	}

	@Override
	public SimpleEntry<Peserta, Peserta> getOldestAndYoungestPeserta(List<Peserta> listPeserta) {
		Comparator<Peserta> cmp = new Comparator<Peserta>() {
		    @Override
		    public int compare(Peserta peserta1, Peserta peserta2) {
		        return peserta1.getTgl_lahir().compareTo(peserta2.getTgl_lahir());
		    }
		};
		
		Peserta youngest = Collections.max(listPeserta, cmp);
		Peserta oldest = Collections.min(listPeserta, cmp);;
		return new SimpleEntry<Peserta, Peserta>(youngest, oldest);
	}

}
