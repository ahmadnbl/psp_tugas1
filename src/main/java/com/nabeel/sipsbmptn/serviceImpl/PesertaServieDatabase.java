package com.nabeel.sipsbmptn.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nabeel.sipsbmptn.dao.PesertaMapper;
import com.nabeel.sipsbmptn.model.Peserta;
import com.nabeel.sipsbmptn.service.PesertaService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PesertaServieDatabase implements PesertaService{
	
	@Autowired
	private PesertaMapper pesertaMapper;
	
	@Override
	public Peserta getPesertaDetail(String nomorPeserta) {
		log.info("Mengambil detail peserta, " + nomorPeserta);
		return pesertaMapper.getPesertaDetail(nomorPeserta);
	}

}
