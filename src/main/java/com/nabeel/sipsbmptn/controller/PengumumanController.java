package com.nabeel.sipsbmptn.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.nabeel.sipsbmptn.service.PengumumanService;

@Controller
@RequestMapping("/pengumuman")
public class PengumumanController {
	
	@Autowired
	PengumumanService pengumumanServiceDatabase;
	
	@RequestMapping("/submit")
	public String showPesertaDetail(Model model, @RequestParam(value = "noPeserta", required = true) String noPeserta){
		model.addAttribute("peserta", pengumumanServiceDatabase.getPesertaStatus(noPeserta));
		model.addAttribute("noPesertaInputted", noPeserta);
		return "pengumuman/hasil";
	}

}
