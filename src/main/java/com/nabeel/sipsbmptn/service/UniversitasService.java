package com.nabeel.sipsbmptn.service;

import java.util.List;

import com.nabeel.sipsbmptn.model.Prodi;
import com.nabeel.sipsbmptn.model.Univ;

public interface UniversitasService {
	
	List<Univ> getUniversitasList();
	
	Univ getUniversitasDetail(String kodeUniv);
	
	List<Prodi> getProdiList(String kodeUniv);

}
