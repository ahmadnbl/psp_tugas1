package com.nabeel.sipsbmptn.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nabeel.sipsbmptn.model.Peserta;
import com.nabeel.sipsbmptn.service.PesertaService;

@RestController
@RequestMapping("/rest/peserta")
public class RestControllerPeserta {
	
	@Autowired
	PesertaService pesertaServiceDatabase;
	
	@RequestMapping(method = RequestMethod.GET)
	public Peserta peserta(Model model,
			@RequestParam(value = "nomor", required = false) String nomorPeserta){
		return pesertaServiceDatabase.getPesertaDetail(nomorPeserta);
	}

}
