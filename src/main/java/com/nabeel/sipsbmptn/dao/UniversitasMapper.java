package com.nabeel.sipsbmptn.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.nabeel.sipsbmptn.model.Prodi;
import com.nabeel.sipsbmptn.model.Univ;

@Mapper
public interface UniversitasMapper {
	
	@Select("SELECT kode_univ, nama_univ FROM univ")
	List<Univ> getUnivList();
	
	@Select("SELECT * FROM univ WHERE kode_univ = #{kodeUniv}")
	Univ getUnivDetailt(@Param("kodeUniv") String kodeUniv);
	
	@Select("SELECT * FROM prodi WHERE kode_univ = #{kodeUniv}")
	List<Prodi> getProdiList(@Param("kodeUniv") String kodeUniv);

}
