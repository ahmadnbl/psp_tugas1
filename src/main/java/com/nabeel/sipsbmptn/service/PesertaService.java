package com.nabeel.sipsbmptn.service;

import com.nabeel.sipsbmptn.model.Peserta;

public interface PesertaService {
	
	Peserta getPesertaDetail(String nomorPeserta);

}
