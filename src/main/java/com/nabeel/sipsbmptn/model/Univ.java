package com.nabeel.sipsbmptn.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Univ {
	private String kode_univ;
	private String nama_univ;
	private String url_univ;
}
