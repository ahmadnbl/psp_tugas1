package com.nabeel.sipsbmptn.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.nabeel.sipsbmptn.model.Peserta;
import com.nabeel.sipsbmptn.service.PesertaService;


@Controller
@RequestMapping("/peserta")
public class PesertaController {
	
	@Autowired
	PesertaService pesertaServiceDatabase;
	
	@RequestMapping(method = RequestMethod.GET)
	public String showDetailProdi(Model model, 
			@RequestParam(value = "nomor", required = false) String nomorPeserta){
		model.addAttribute("nomorPesertaInputted", nomorPeserta);
		
		Peserta peserta = pesertaServiceDatabase.getPesertaDetail(nomorPeserta);
		model.addAttribute("peserta", peserta);
		
		return "peserta/detail_peserta";
	}

}
