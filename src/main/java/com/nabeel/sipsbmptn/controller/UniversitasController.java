package com.nabeel.sipsbmptn.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.nabeel.sipsbmptn.model.Prodi;
import com.nabeel.sipsbmptn.model.Univ;
import com.nabeel.sipsbmptn.service.UniversitasService;

@Controller
@RequestMapping("/univ")
public class UniversitasController {
	
	@Autowired
	private UniversitasService universitasServiceDatabase;
	
	@RequestMapping("")
	public String getUniversitasList(Model model){
		model.addAttribute("daftarUniversitas", universitasServiceDatabase.getUniversitasList());
		return "universitas/daftar_universitas";
	}
	
	@RequestMapping("/{kodeUniv}")
	public String getUniversitasDetail(Model model, 
			@PathVariable(value = "kodeUniv", required = true) String kodeUniv){
		model.addAttribute("kodeUnivInputted", kodeUniv);
		
		Univ currentUniv = universitasServiceDatabase.getUniversitasDetail(kodeUniv);
		model.addAttribute("universitas", currentUniv);
		
		List<Prodi> prodiList = new ArrayList<>();
		
		if (currentUniv != null){
			prodiList.addAll(universitasServiceDatabase.getProdiList(kodeUniv));
		}
		model.addAttribute("daftarProdi", prodiList);
		
		return "universitas/detail_universitas";
	}

}
