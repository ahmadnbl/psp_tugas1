package com.nabeel.sipsbmptn.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nabeel.sipsbmptn.model.Prodi;
import com.nabeel.sipsbmptn.service.ProdiService;

@RestController
@RequestMapping("/rest/prodi")
public class RestControllerProdi {
	
	@Autowired
	ProdiService prodiServiceDatabase;
	
	@RequestMapping(method = RequestMethod.GET)
	public Prodi prodi(Model model,
			@RequestParam(value = "kode", required = false) String kodeProdi){
		return prodiServiceDatabase.getProdiDetail(kodeProdi);
	}

}
