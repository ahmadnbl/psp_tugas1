package com.nabeel.sipsbmptn.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Prodi {
	private String kode_prodi;
	private String nama_prodi;
	private Univ Universitas;
}
