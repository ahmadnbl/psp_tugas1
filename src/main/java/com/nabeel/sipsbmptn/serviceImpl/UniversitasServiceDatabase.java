package com.nabeel.sipsbmptn.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nabeel.sipsbmptn.dao.UniversitasMapper;
import com.nabeel.sipsbmptn.model.Prodi;
import com.nabeel.sipsbmptn.model.Univ;
import com.nabeel.sipsbmptn.service.UniversitasService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UniversitasServiceDatabase implements UniversitasService{
	
	@Autowired
	UniversitasMapper universitasMapper;

	@Override
	public List<Univ> getUniversitasList() {
		log.info("Mengmabil daftar universitas");
		return universitasMapper.getUnivList();
	}

	@Override
	public Univ getUniversitasDetail(String kodeUniv) {
		log.info("Mengmabil detail universitas, " + kodeUniv);
		return universitasMapper.getUnivDetailt(kodeUniv);
	}

	@Override
	public List<Prodi> getProdiList(String kodeUniv) {
		log.info("Mengmabil daftar prodi dari universitas, " + kodeUniv);
		return universitasMapper.getProdiList(kodeUniv);
	}

}
