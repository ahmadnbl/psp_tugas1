package com.nabeel.sipsbmptn.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.nabeel.sipsbmptn.model.Peserta;

@Mapper
public interface PesertaMapper {
	
	@Select("SELECT * FROM peserta WHERE nomor = #{nomorPeserta}")
	Peserta getPesertaDetail(@Param("nomorPeserta") String nomorPeserta);

}
