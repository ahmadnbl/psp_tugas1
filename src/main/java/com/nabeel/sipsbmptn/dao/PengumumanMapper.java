package com.nabeel.sipsbmptn.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import com.nabeel.sipsbmptn.model.Peserta;

@Mapper
public interface PengumumanMapper {
	
//	SELECT 
//	r.nomor,
//    p.nama,
//    p.tgl_lahir,
//    r.kode_prodi,
//    r.nama_prodi,
//    r.kode_univ, 
//    r.nama_univ,
//    r.url_univ
//FROM 
//	result r
//LEFT JOIN peserta p
//	ON p.nomor = r.nomor
//LEFT JOIN prodi prod
//	ON prod.kode_prodi = r.kode_prodi
//LEFT JOIN univ u
//	ON u.kode_univ = r.kode_univ;
    
//SELECT 
//	r.nomor,
//    p.nama,
//    p.tgl_lahir,
//    r.kode_prodi,
//    r.nama_prodi,
//    r.kode_univ, 
//    r.nama_univ,
//    r.url_univ
//FROM 
//	result r,
//    peserta p,
//    prodi prod,
//    univ u
//WHERE
//	p.nomor = r.nomor AND 
//    prod.kode_prodi = r.kode_prodi AND 
//    u.kode_univ = r.kode_univ;

//SELECT 
//	r.nomor,
//    p.nama,
//    p.tgl_lahir,
//    r.kode_prodi,
//    r.nama_prodi,
//    r.kode_univ, 
//    r.nama_univ,
//    r.url_univ
//FROM 
//	result r
//JOIN peserta p
//	ON p.nomor = r.nomor
//JOIN prodi prod
//	ON prod.kode_prodi = r.kode_prodi
//JOIN univ u
//	ON u.kode_univ = r.kode_univ;
	
	@Select("SELECT"
			+ "		p.nomor,"
		    + "		p.nama,"
		    + "		p.tgl_lahir,"
		    + "		prod.kode_prodi,"
		    + "		prod.nama_prodi,"
		    + "		u.kode_univ, "
		    + "		u.nama_univ,"
		    + "		u.url_univ "
		    + "FROM"
		    + "		peserta p "
		    + "LEFT JOIN prodi prod "
		    + "		ON prod.kode_prodi = p.kode_prodi "
		    + "LEFT JOIN univ u "
		    + "		ON u.kode_univ = prod.kode_univ "
		    + "WHERE"
		    + "		p.nomor = #{nomorPeserta} ;")
	@Results(value = {
			@Result(property="nomor", column="nomor"), 
			@Result(property="nama", column="nama"), 
			@Result(property="tgl_lahir", column="tgl_lahir"),
			@Result(property="prodi.kode_prodi", column="kode_prodi"),
			@Result(property="prodi.nama_prodi", column="nama_prodi"),
			@Result(property="prodi.Universitas.kode_univ", column="kode_univ"),
			@Result(property="prodi.Universitas.nama_univ", column="nama_univ"),
			@Result(property="prodi.Universitas.url_univ", column="url_univ")
			})
	Peserta getHasilPengumumanByNomor(@Param("nomorPeserta") String nomorPeserta);

}
