package com.nabeel.sipsbmptn.model;

import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Peserta {
	private String nomor;
	private String nama;
	private Date tgl_lahir;
	private Prodi prodi;
	
	public int getUmur(){
		LocalDate birthdate = tgl_lahir.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		LocalDate now = LocalDate.now();
		return Period.between(birthdate, now).getYears();
	}
}
